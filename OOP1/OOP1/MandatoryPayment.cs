﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    class MandatoryPayment : Payment
    {
        private float taxPerCent;

        protected MandatoryPayment(decimal sum, ValueType type, float taxPerCent) : base(sum, type)
        {
            this.taxPerCent = taxPerCent;
        }
    }
}
