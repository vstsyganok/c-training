﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    public abstract class Department
    {
        protected String departmentName;

        protected Department(string departmentName)
        {
            this.departmentName = departmentName ?? throw new ArgumentNullException(nameof(departmentName));
        }
    }
}
