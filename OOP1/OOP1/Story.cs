﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    class Story : Publication
    {
        protected Story(string name, string book, decimal price, int yearOfPublishing, string publisherName, string address, BookType type) : base(name, book, price, yearOfPublishing, publisherName, address, type)
        {
            Console.WriteLine("Это рассказ");
        }
    }
}
