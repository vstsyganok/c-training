﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    abstract class Payment
    {
        public enum ValutType { USD, RUR, EUR };

        protected decimal sum;
        protected ValueType type;

        protected Payment(decimal sum, ValueType type)
        {
            this.sum = sum;
            this.type = type ?? throw new ArgumentNullException(nameof(type));
        }
    }
}
