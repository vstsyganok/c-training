﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    class SalesDepartment : Department
    {
        private DateTime saleDate;
        private int count;
        private decimal saleSum;
        private List<Payment> payments;
        private List<Publication> publications;

        protected SalesDepartment(string departmentName, DateTime saleDate, int count, decimal saleSum) : base(departmentName)
        {
            this.saleDate = saleDate;
            this.count = count;
            this.saleSum = saleSum;
            payments = new List<Payment>();
            publications = new List<Publication>();
        }
    }
}
