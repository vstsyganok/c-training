﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    abstract class Publication
    {
        public enum BookType { HISTORICAL, FANTASY, NOVEL, DETECTIVE }

        protected String name;
        protected String book;
        protected decimal price;
        protected int yearOfPublishing;
        protected String publisherName;
        protected String address;
        protected BookType type;
        private Dictionary<String, Author> authors;
        private Dictionary<String, Publisher> publishers;

        protected Publication(string name, string book, decimal price, int yearOfPublishing, string publisherName, string address, BookType type)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.book = book ?? throw new ArgumentNullException(nameof(book));
            this.price = price;
            this.yearOfPublishing = yearOfPublishing;
            this.publisherName = publisherName ?? throw new ArgumentNullException(nameof(publisherName));
            this.address = address ?? throw new ArgumentNullException(nameof(address));
            this.type = type;
            authors = new Dictionary<string, Author>();
            publishers = new Dictionary<string, Publisher>();
        }
    }
}
