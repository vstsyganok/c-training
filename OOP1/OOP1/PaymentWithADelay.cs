﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP1
{
    class PaymentWithADelay : Payment
    {
        private String delayTime;

        protected PaymentWithADelay(decimal sum, ValueType type, String delayTime) : base(sum, type)
        {
            this.delayTime=delayTime;
        }
    }
}
