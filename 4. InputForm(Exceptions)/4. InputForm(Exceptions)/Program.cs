﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.InputForm__exceptions_
{
    class Program
    {
        static void Main(string[] args)
        {

            string taskName;
            string taskDescription;
            int taskTime;
            DateTime startDate;
            DateTime endDate;
            Boolean correctPeriod = false;

            taskName = enterTaskName();
            taskDescription = enterTaskDescription();
            taskTime = enterTaskTime();
            startDate = enterStartDate();
            endDate = enterEndDate(startDate);
            correctPeriod = checkCorrectPeriods(startDate, endDate, taskTime);

            while (correctPeriod == false)
            {
                try
                {
                    throw new IncorrectPeriodsException();
                }
                catch
                {
                    Console.WriteLine("ENDDATE - STARTDATE <> TASKTIME. PLEASE TRY AGAIN");
                    taskTime = enterTaskTime();
                    startDate = enterStartDate();
                    endDate = enterEndDate(startDate);
                    correctPeriod = checkCorrectPeriods(startDate, endDate, taskTime);
                }
            }

            Console.WriteLine("Имя задачи: {0}, Дата начала задачи: {1}, Дата окончания задачи: {2}, Оценка по времени: {3}, Описание: {4}", taskName, startDate, endDate, taskTime, taskDescription);
            Console.ReadKey();
        }

        static string enterTaskName()
        {
            string taskName;
            Console.WriteLine("Enter task name");

            taskName = Console.ReadLine();

            if (String.IsNullOrEmpty(taskName))
            {
                return enterTaskName();
            }
            return taskName;
        }
        static string enterTaskDescription()
        {
            string taskDescription;
            Console.WriteLine("Enter task description");

            taskDescription = Console.ReadLine();

            if (String.IsNullOrEmpty(taskDescription))
            {
                return enterTaskDescription();
            }
            return taskDescription;
        }
        static int enterTaskTime()

        {
            int taskTime;
            Console.WriteLine("Enter task time in days");

            try
            {
                taskTime = Convert.ToInt32(Console.ReadLine());

                if (taskTime < 0)
                {
                    Console.WriteLine("Task time should be > 0");
                    return enterTaskTime();
                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Wrong format of TaskTime");
                return enterTaskTime();
            }

            return taskTime;
        }
        static DateTime enterStartDate()
        {
            DateTime startDate = new DateTime(2010, 1, 8);

            Console.WriteLine("Enter start date in format DD.MM.YYYY");
            while (!DateTime.TryParse(Console.ReadLine(), out startDate))
            {
                Console.WriteLine("Wrong format of start date (format DD.MM.YYYY)");
                Console.WriteLine("Input start date in format DD.MM.YYYY");
            }
            return startDate;
        }
        static DateTime enterEndDate(DateTime startDate)
        {
            DateTime endDate = new DateTime(2010, 1, 8);

            Console.WriteLine("Enter end date in format DD.MM.YYYY");
            while (!DateTime.TryParse(Console.ReadLine(), out endDate))
            {
                Console.WriteLine("Wrong format of end date (format DD.MM.YYYY)");
                Console.WriteLine("Input end date in format DD.MM.YYYY");
            }
            if (endDate < startDate)
            {
                Console.WriteLine("End date can`t be less then start date");
                return enterEndDate(startDate);
            }
            return endDate;
        }
        static Boolean checkCorrectPeriods(DateTime startDate, DateTime endDate, int taskTime)
        {
            TimeSpan time = endDate - startDate;
            if (taskTime == time.TotalDays+1)
            {
                return true;
            }
            else
                return false;
        }
    }
}