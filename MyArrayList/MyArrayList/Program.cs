﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArrayList
{
    class Program
    {
       
        static void Main(string[] args)
        {
            MyArrayList myAr = new MyArrayList();
            int[] list = new int[3];
            int index;
            bool st;
            int getItem;
            int getIndex;
            list[0] = 0;
            list[1] = 1;
            list[2] = 2;

            list = myAr.AddElement(list, 3);

            for (int i = 0; i < list.Length; i++)
            {
                Console.WriteLine(list[i]);
            }

            Console.WriteLine("*********************************************");

            list = myAr.removeElementByIndex(list, 3);
            for (int i = 0; i < list.Length; i++)
            {
                Console.WriteLine(list[i]);
            }

            Console.WriteLine("*********************************************");
            index = myAr.indexOfElement(list, 10);
            Console.WriteLine(index);

            Console.WriteLine("*********************************************");

            st = myAr.removeElement(ref list, 1);

            Console.WriteLine(st);
            for (int i = 0; i < list.Length; i++)
            {
                Console.WriteLine(list[i]);
            }

            getIndex = myAr.getElementByIndex(list, 10);

            Console.WriteLine(getIndex);

            getItem = myAr.getElement(list, 1);

            Console.WriteLine(getItem);
            Console.ReadKey();
        }
    }
}
