﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArrayList
{
    public class MyArrayList
    {
        public int[] AddElement(int[] array, int item)
        {
            Array.Resize<int>(ref array, array.Length + 1);
            array[array.Length - 1] = item;
            return array;
        }

        public int[] removeElementByIndex(int[] array, int index)
        {
            int[] newArray = new int[array.Length - 1];

            for (int i = 0; i < array.Length; i++)
            {
                if (i < index)
                {
                    newArray[i] = array[i];
                }
                else if (i > index)
                {
                    newArray[i - 1] = array[i];
                }
            }
            return newArray;
        }

        public int indexOfElement(int[] array, int item)
        {
            //int[] newArray = new int[array.Length - 1];

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == item)
                {
                    return i;
                }
            }
            return -1;
        }

        public bool removeElement(ref int[] array, int item)
        {
            int temp = indexOfElement(array, item);
            if (temp == -1)
                return false;
            
            array=removeElementByIndex(array, temp);
            return true;
        }

        public int getElementByIndex(int[] array, int index)
        {
            //int[] newArray = new int[array.Length - 1];

            for (int i = 0; i < array.Length; i++)
            {
                if (i == index)
                {
                    return array[i];
                }
            }
            return -1;
        }

        public int getElement(int[] array, int item)
        {
            //int[] newArray = new int[array.Length - 1];

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == item)
                {
                    return array[i];
                }
            }
            return -1;
        }



    }


}
