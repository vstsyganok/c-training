﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            OrganizationCatalog catalog = new OrganizationCatalog();

            Organization organization1 = new Organization("Roga i kopyta1", 1, new string[] { "555", "222" }.ToList(), "Omsk", 3.5m);
            Organization organization2 = new Organization("Roga i kopyta2", 2, new string[] { "333", "444" }.ToList(), "Moscow", 1.5m);
            Organization organization3 = new Organization("Roga i kopyta3", 2, new string[] { "555", "666" }.ToList(), "Omsk", 2.5m);
            Organization organization4 = new Organization("Roga i kopyta3", 2, new string[] { "555", "666" }.ToList(), "Omsk", 0.5m);

            catalog.organizations.Add(organization1);
            catalog.organizations.Add(organization2);
            catalog.organizations.Add(organization3);
            catalog.organizations.Add(organization4);

            IEnumerable<DZ_LINQ.Organization> organizationList = null;

            organizationList = (IEnumerable<Organization>)catalog.organizations;

            do
            {
                Console.WriteLine("\n Выберите операцию: \n 1 - Сортировать по оборотному капиталу \n 2 - Выбрать по городу \n 3 - Выбрать по ОКВЭД \n 4 - Завершить работу");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("\n Выберите тип сортировки: \n 1 - Сортировать по возрастанию \n 2 - Сортировать по убыванию \n 4 - Завершить работу \n");
                        int choice2 = Convert.ToInt32(Console.ReadLine());
                        if (choice2 == 2)
                        {
                            organizationList = SortByDesc(organizationList);
                            break;
                        }
                        else if (choice2 == 4)
                        {
                            Environment.Exit(0);
                            break;
                        }

                        organizationList = SortByAsc(organizationList);
                        break;

                    case 2:
                        Console.WriteLine("Введите название города");
                        string city = Console.ReadLine();

                        organizationList = sortByCity(city, organizationList);
                        break;

                    case 3:
                        Console.WriteLine("Введите код ОКВЭД");
                        string code = Console.ReadLine();

                        organizationList = sortByCode(code, organizationList);
                        break;

                    case 4:
                        Environment.Exit(0);
                        break;
                }

            } while (true);

            }

        public static IEnumerable<DZ_LINQ.Organization> SortByAsc(IEnumerable<DZ_LINQ.Organization> organizationList)
        {
            //По возрастанию капитала
            var sortASC = organizationList.OrderBy(o => o.capital);
            printOrganizationsEnumerable((IEnumerable<Organization>)sortASC);
            return sortASC;
        }

        public static IEnumerable<DZ_LINQ.Organization> SortByDesc(IEnumerable<DZ_LINQ.Organization> organizationList)
        {
            //По убыванию капитала
            var sortDESC = organizationList.OrderByDescending(o => o.capital);
            printOrganizationsEnumerable((IEnumerable<Organization>)sortDESC);
            return sortDESC;
        }

        public static IEnumerable<DZ_LINQ.Organization> sortByCity(string city,IEnumerable<DZ_LINQ.Organization> organizationList)
        {
            //По городу
            var onCity = organizationList.Where(o => o.address.Contains(city));
            printOrganizationsEnumerable((IEnumerable<Organization>)onCity);
            return onCity;
        }

        public static IEnumerable<DZ_LINQ.Organization> sortByCode(string code, IEnumerable<DZ_LINQ.Organization> organizationList)
        {
            //По коду
            var onCode = organizationList.Where(o => o.okvad.Contains(code));
            printOrganizationsEnumerable((IEnumerable<Organization>)onCode);
            return onCode;
        }

        public static void printOrganizationsEnumerable(IEnumerable<Organization> organizations)
        {
            foreach (Organization organization in organizations)
            {
                Console.WriteLine(organization.ToString());
            }
        }
    }
}
