﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_LINQ
{
    public class OrganizationCatalog
    {
        public List<Organization> organizations;

        public OrganizationCatalog(List<Organization> organizations)
        {
            this.organizations = organizations ?? throw new ArgumentNullException(nameof(organizations));
        }

        public OrganizationCatalog()
        {
            organizations = new List<Organization>();
        }

    }
}
