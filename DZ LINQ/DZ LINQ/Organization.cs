﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_LINQ
{
    public class Organization
    {
        public String name;
        public int INN;
        public List<String> okvad; 
        public String address;
        public decimal capital;

        public Organization(string name, int INN, List<string> okvad, string address, decimal capital)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.INN = INN;
            this.okvad = okvad ?? throw new ArgumentNullException(nameof(okvad));
            this.address = address ?? throw new ArgumentNullException(nameof(address));
            this.capital = capital;
        }



        public override string ToString()
        {
            string strOkvad = string.Join(",", okvad);
            return String.Format("Organization name: {0}, INN:  {1}, Address: {2}, Capital: {3}, Okvads: {4}", name, INN, address, capital, strOkvad);
        }
    }
}
