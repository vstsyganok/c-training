﻿using System;

namespace HotColdGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int predefinedNumber = 50;
            int lastNumber;
            int inputNumber;
            int attempt = 1;

            runCycle();

            void runCycle()
            {
                Console.WriteLine("Попытка №{0}. " + "Введите первое число от 1 до 100 еще раз", attempt);
                lastNumber = Convert.ToInt32(Console.ReadLine());

                while (predefinedNumber != lastNumber)
                {
                    attempt++;
                    Console.WriteLine("Попытка №{0}. " + "Введите новое число", attempt);
                    inputNumber = Convert.ToInt32(Console.ReadLine());

                    if (inputNumber == predefinedNumber)
                    {
                        break;
                    }
                    else if (inputNumber == lastNumber)
                    {
                        Console.WriteLine("Введи число отличное от предыдущего");
                    }
                    else if (Math.Abs(predefinedNumber - inputNumber) < Math.Abs(predefinedNumber - lastNumber))
                    {
                        Console.WriteLine("*** Горячо ***");
                    }
                    else if (Math.Abs(predefinedNumber - inputNumber) > Math.Abs(predefinedNumber - lastNumber))
                    {
                        Console.WriteLine("*** Холодно ***");
                    }
                    else
                    {
                        Console.WriteLine("*** Не горячо и не холодно, также 😊 ***");
                    }
                    lastNumber = inputNumber;
                }
                Console.WriteLine("*** Успех! ***");
                Console.ReadKey();
            }
        }
    }
}