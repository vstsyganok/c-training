﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class DishElement : WebComposite
    {

        public IWebElement orderButton;
        public IWebElement price;
        private const string PRICE_BUTTON_PATH = @"./form/p/strong/span[1]";
        private const string ORDER_BUTTON_PATH = @"./form/p/a";


        public DishElement(IWebElement fromElement) : base(fromElement)
        {

            orderButton = fromElement.FindElement(By.XPath(ORDER_BUTTON_PATH));
            price = fromElement.FindElement(By.XPath(PRICE_BUTTON_PATH));

        }
    }
}
