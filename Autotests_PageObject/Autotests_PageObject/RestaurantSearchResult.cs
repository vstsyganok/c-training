﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class RestaurantSearchResult : PageObject
    {

        public List<RestaurantResultsElement> restaurantResultElements;
        private const string RESTAURANT_LIST_XPATH = "//ul/li[@class='vendor-item']/section";

        public RestaurantSearchResult(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            restaurantResultElements = new List<RestaurantResultsElement>();

            foreach (IWebElement webElem in driver.FindElements(By.XPath(RESTAURANT_LIST_XPATH)))
            {
                RestaurantResultsElement restaurantResult = new RestaurantResultsElement(webElem);
                restaurantResultElements.Add(restaurantResult);
            }
        }

        public RestaurauntDishesPage openRestaurant(string restaurantName)
        {

            foreach (var item in restaurantResultElements)
            {
                if (restaurantName.Equals(item.getHeaderText()))
                {
                    item.restaurantHeader.Click();
                    return new RestaurauntDishesPage(driver.Url, driver);
                }
            }

            throw new ArgumentNullException();
        }
    }
}
