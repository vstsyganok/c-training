﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class ArticleResultsElement : WebComposite
    {
        public IWebElement articleHeader;

        private const string HEADER_PATH_IN_ARTICLE = "//div[@class='blog-post']/h2/a";

        public ArticleResultsElement(IWebElement fromElement) : base(fromElement)
        {
            articleHeader = fromElement.FindElement(By.XPath(HEADER_PATH_IN_ARTICLE));
        }

        public string getHeaderText() {
            return articleHeader.Text;
        }

    }
}
