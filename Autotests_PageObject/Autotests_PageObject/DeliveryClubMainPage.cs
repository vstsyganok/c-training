﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class DeliveryClubMainPage : PageObject
    {
        public IWebElement searchString;
        public IWebElement findButton;
        public IWebElement validateMessage;
        public IWebElement blogButton;

        public FooterMenu footerMenu;

        public enum MenuEntity
        {
            ABOUT_US,
            FOR_PRESS,
            PRIZZES,
            BLOG
        }

        public string SEARCH_STRING_ID = "user-addr__input";
        private const string FIND_BUTTON_XPATH = "//a[contains(.,'Найти рестораны')]";
        public string FIND_VALIDATE_MESSAGE = "//span[@class='tooltip-v2-par']/span[@class='tooltip-v2 tooltip-v2- tooltip-v2-addr-input tooltip-v2__error']";
        private const string BLOG_BUTTON_XPATH = "//div[@class='footer-menu']/a[.//h2[contains(.,'Блог')]]";
        private const string FOOTER_MENU_XPATH = "//div[@class='footer-menu']";
        private const string ABOUT_US_MENU_XPATH = "//div[@class='footer-menu']/a/h2[contains(text(),'нас')]/parent::a";
        private const string PRIZZES_MENU_XPATH = "//div[@class='footer-menu']/a/h2[contains(text(),'Призы')]/parent::a";
        private const string BLOG_MENU_XPATH = "//div[@class='footer-menu']/a/h2[contains(text(),'Блог')]/parent::a";
        private const string PRESS_MENU_XPATH = "//div[@class='footer-menu']/a/h2[contains(text(),'прессы')]/parent::a";
        public const string correctValidateMessage = "Пожалуйста, уточните ваш адрес";


        public DeliveryClubMainPage(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            searchString = driver.FindElement(By.Id(SEARCH_STRING_ID));
            findButton = driver.FindElement(By.XPath(FIND_BUTTON_XPATH));
            validateMessage = driver.FindElement(By.XPath(FIND_VALIDATE_MESSAGE));
            footerMenu = new FooterMenu(driver.FindElement(By.XPath(FOOTER_MENU_XPATH)));
        }

        public PageObject clickOnFooterMenu(MenuEntity choice)
        {
            IWebElement menuItemToClick = null;
            
            switch (choice)
            {
                case MenuEntity.ABOUT_US:
                    menuItemToClick = driver.FindElement(By.XPath(ABOUT_US_MENU_XPATH));
                    menuItemToClick.Click();
                    throw new NotImplementedException();

                case MenuEntity.FOR_PRESS:
                    menuItemToClick = driver.FindElement(By.XPath(PRESS_MENU_XPATH));
                    menuItemToClick.Click();
                    throw new NotImplementedException();

                case MenuEntity.PRIZZES:
                    menuItemToClick = driver.FindElement(By.XPath(PRIZZES_MENU_XPATH));
                    menuItemToClick.Click();
                    throw new NotImplementedException();

                case MenuEntity.BLOG:
                    menuItemToClick = driver.FindElement(By.XPath(BLOG_MENU_XPATH));
                    menuItemToClick.Click();
                    return new BlogPage(driver.Url, driver);

                default:
                    return new DeliveryClubMainPage("https://www.delivery-club.ru", driver);
                    throw new NotImplementedException();
            }

        }

        public RestaurantSearchResult runSearchByAddress(string address)
        {            

            if (!searchString.Text.Equals(""))
            {
                searchString.Clear();
            }

            //searchString.Click();
            searchString.SendKeys(address + Keys.Tab);
            findButton.Click();
            return new RestaurantSearchResult(driver.Url,driver);
        }

    }
}
