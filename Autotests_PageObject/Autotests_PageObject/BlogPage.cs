﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Autotests_PageObject
{
    public class BlogPage : PageObject
    {
        public IWebElement searchString;

        private string ARTICLE_RESULT_CLASS = @"blog-post";

        public List<ArticleResultsElement> articleResultElements;

        private const string SEARCH_STRING_ID = "s";
        private const string ARTICLE_LIST_XPATH = "//div[@id='content']/div/h2/a";

        public BlogPage(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            searchString = driver.FindElement(By.Id(SEARCH_STRING_ID));

            articleResultElements = new List<ArticleResultsElement>();
            foreach (IWebElement webElem in driver.FindElements(By.ClassName(ARTICLE_RESULT_CLASS)))
            {
                ArticleResultsElement articleResult = new ArticleResultsElement(webElem);
                articleResultElements.Add(articleResult);
            }
        }

        public ArticlePage openTheArticle(string articleName)
        {
            articleResultElements = new List<ArticleResultsElement>();

            foreach (IWebElement webElem in driver.FindElements(By.ClassName(ARTICLE_RESULT_CLASS)))
            {
                ArticleResultsElement articleResult = new ArticleResultsElement(webElem);
                articleResultElements.Add(articleResult);
            }

            foreach (var item in articleResultElements)
            {
                if (articleName.Equals(item.getHeaderText()))
                {
                    item.articleHeader.Click();
                    return new ArticlePage(driver.Url, driver);
                }

            }

            throw new ArgumentNullException();
        }

    }
}
