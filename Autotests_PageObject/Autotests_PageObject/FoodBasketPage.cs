﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class FoodBasketPage : PageObject
    {

        public IWebElement doOrderButton;
        public string DO_ORDER_BUTTON_PATH = @"//div[@class='order-info__button-order']/a";


        public FoodBasketPage(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            doOrderButton = driver.FindElement(By.XPath(DO_ORDER_BUTTON_PATH));
        }
    }
}
