﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Autotests_PageObject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CheckValidationOnMainPage1()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver\");

            DeliveryClubMainPage mainPage =
            new DeliveryClubMainPage("https://www.delivery-club.ru/", driver);

            if (!mainPage.searchString.Text.Equals(""))
            {
                mainPage.searchString.Clear();
            }

            mainPage.findButton.Click();

            WebDriverWait ww = new WebDriverWait(driver,TimeSpan.FromSeconds(10));

            ww.Until(ExpectedConditions.ElementIsVisible(By.XPath(mainPage.FIND_VALIDATE_MESSAGE)));

            Assert.AreEqual(DeliveryClubMainPage.correctValidateMessage, mainPage.validateMessage.Text);
            driver.Quit();
        }

        [TestMethod]
        public void AddCommentInBlog()
        {
            const string searchRequest = "Delivery Club: Топ-5 самых страшных и странных блюда мира";
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver\");
            DeliveryClubMainPage mainPage =
            new DeliveryClubMainPage("https://www.delivery-club.ru/", driver);

            BlogPage bp = (BlogPage)mainPage.clickOnFooterMenu(DeliveryClubMainPage.MenuEntity.BLOG);
            bp.searchString.SendKeys(searchRequest + Keys.Enter);

            ArticlePage ap = bp.openTheArticle(searchRequest);

            ap.nameField.SendKeys("Vladimir");
            ap.emailField.SendKeys("tsyganokvs@gmail.com");
            ap.commentField.SendKeys("Some Text");
            ap.submitButton.Click();

            driver.Quit();
        }

        [TestMethod]
        public void WithLowSumMakeAnOrder()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver\");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

            DeliveryClubMainPage mainPage =
            new DeliveryClubMainPage("https://www.delivery-club.ru/", driver);
            
            RestaurantSearchResult rsr = mainPage.runSearchByAddress("Омск, Мельничная улица, 58В");
            RestaurauntDishesPage rp = rsr.openRestaurant("Ананас");
            
            FoodBasketPage fbp = rp.moveToBasketWithLackSum();

            bool visible = fbp.doOrderButton.Displayed;
            Assert.IsFalse(visible);

            driver.Quit();
        }

        [TestMethod]
        public void MakeAnOrder()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver\");

            WebDriverWait ww = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);

            DeliveryClubMainPage mainPage =
            new DeliveryClubMainPage("https://www.delivery-club.ru/", driver);

            RestaurantSearchResult rsr = mainPage.runSearchByAddress("Омск, Мельничная улица, 58В");
            RestaurauntDishesPage rp = rsr.openRestaurant("Ананас");

            FoodBasketPage fbp = rp.moveToBasket();

            bool visible = fbp.doOrderButton.Displayed;
            Assert.IsTrue(visible);

            driver.Quit();
        }
    }
}
