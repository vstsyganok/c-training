﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class FooterMenu : WebComposite
    {
        private const string FEED_LIVE_ITEM_PATH = @"//ul[@id='feed-live']/li";

        public FooterMenu(IWebElement fromElement) : base(fromElement)
        {

        }
    }
}
