﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotests_PageObject
{
    public abstract class WebComposite
    {
        protected IWebElement fromElement;

        protected WebComposite(IWebElement fromElement)
        {
            this.fromElement = fromElement ?? throw new ArgumentNullException(nameof(fromElement));

        }

    }
}
