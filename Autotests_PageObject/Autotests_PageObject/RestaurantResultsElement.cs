﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autotests_PageObject
{
    public class RestaurantResultsElement : WebComposite
    {
        public IWebElement restaurantHeader;
        private const string RESTAURANT_HEADER_LIST_XPATH = @"./div/section/a";
        
        public RestaurantResultsElement(IWebElement fromElement) : base(fromElement)
        {
            restaurantHeader = fromElement.FindElement(By.XPath(RESTAURANT_HEADER_LIST_XPATH));
        }

        public string getHeaderText()
        {
            return fromElement.FindElement(By.XPath(RESTAURANT_HEADER_LIST_XPATH)).Text;
        }
    }
}
