﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class ArticlePage : PageObject
    {
        public IWebElement commentField;
        public IWebElement nameField;
        public IWebElement emailField;
        public IWebElement submitButton;

        private const string COMMENT_FIELD_ID = "comment";
        private const string NAME_FIELD_ID = "author";
        private const string EMAIL_FIELD_ID = "email";
        private const string SUBMIT_BUTTON_ID = "submit";


        public ArticlePage(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            commentField = driver.FindElement(By.Id(COMMENT_FIELD_ID));
            nameField = driver.FindElement(By.Id(NAME_FIELD_ID));
            emailField = driver.FindElement(By.Id(EMAIL_FIELD_ID));
            submitButton = driver.FindElement(By.Id(SUBMIT_BUTTON_ID));
        }
    }
}
