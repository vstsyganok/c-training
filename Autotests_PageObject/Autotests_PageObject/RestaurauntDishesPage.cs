﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Autotests_PageObject
{
    public class RestaurauntDishesPage : PageObject
    {

        int minSum;
        const string MIN_SUM_ORDER_XPATH = "//div[@class='vendor-headline__info']/span/span";

        int currentSum;
        const string CURRENT_SUM_ORDER_XPATH = "//span/span[@class='basket_sum']";

        IWebElement basketButton;
        const string BASKET_BUTTON_PATH = "//div[@class='cart_2 from_basket']/a[@class='button alt open_basket from_basket']";

        public List<DishElement> dishElements;
        private const string DISHES_LIST_XPATH = "//ul[@class='dish_list']/li";

        public RestaurauntDishesPage(string currentUrl, IWebDriver driver) : base(currentUrl, driver)
        {
            minSum = Convert.ToInt32(driver.FindElement(By.XPath(MIN_SUM_ORDER_XPATH)).Text);

            dishElements = new List<DishElement>();
            foreach (IWebElement webElem in driver.FindElements(By.XPath(DISHES_LIST_XPATH)))
            {
                DishElement dishResult = new DishElement(webElem);
                dishElements.Add(dishResult);
            }

        }

        public FoodBasketPage moveToBasketWithLackSum()
        {

            foreach (var dishElement in dishElements)
            {
                if (Convert.ToInt32(dishElement.price.Text) < minSum)
                {
                    dishElement.orderButton.Click();
                    break;
                }
            }

            basketButton = driver.FindElement(By.XPath(BASKET_BUTTON_PATH));
            currentSum = Convert.ToInt32(driver.FindElement(By.XPath(CURRENT_SUM_ORDER_XPATH)).Text);

            if (currentSum < minSum)

            {
                basketButton.Click();
                return new FoodBasketPage(driver.Url, driver);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public FoodBasketPage moveToBasket()
        {
            

            foreach (var dishElement in dishElements)
            {
                dishElement.orderButton.Click();
                currentSum = Convert.ToInt32(driver.FindElement(By.XPath(CURRENT_SUM_ORDER_XPATH)).Text);

                if (currentSum >= minSum)
                {
                    break;
                }
            }

            basketButton = driver.FindElement(By.XPath(BASKET_BUTTON_PATH));
            basketButton.Click();

            return new FoodBasketPage(driver.Url, driver);

        }
    }
}
