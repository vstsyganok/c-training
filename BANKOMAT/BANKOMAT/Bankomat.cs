﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BANKOMAT
{
    class Bankomat
    {
        List<Account> accounts = new List<Account>();

        public void identifyCustomer(out int clientId, out int accountNumber, out bool hasUser, out bool hasDept, out bool hasAccount)
        {

            do
            {
                Console.WriteLine("Введите свой ID");
                clientId = Convert.ToInt32(Console.ReadLine());
                hasUser=checkHasUserMethod(clientId);
            } while (hasUser == false);

            do
            {
                Console.WriteLine("Введите свой номер счета или нажмите 0 чтобы выйти");
                accountNumber = Convert.ToInt32(Console.ReadLine());
                if (accountNumber == 0)
                {
                    Console.WriteLine("Пока");
                    System.Threading.Thread.Sleep(2000);
                    Environment.Exit(0);
                }
                hasDept = checkHasDeptMethod(clientId, accountNumber);
                hasAccount = checkHasAccountMethod(clientId, accountNumber);

                if (hasDept == true)
                {
                    Console.WriteLine("Пока");
                    System.Threading.Thread.Sleep(2000);
                    Environment.Exit(0);
                }

            } while (hasAccount == false);
        }



        public void addAccount(Account account)
        {
            accounts.Add(account);
        }

        public void printAllAccounts()
        {
            foreach (Account account in accounts)
            {
                Console.WriteLine(account);
            }
        }

        public bool checkHasDeptMethod(int clientId, int accountNumber)
        {
            foreach (Account account in accounts)
            {
                if (account.ClientId == clientId && account.AccountNumber == accountNumber && account.accountType.Equals(Account.AccountType.DEBIT))
                {
                    foreach (Account account2 in accounts)
                    {
                        if (account2.ClientId == clientId && account2.Balance <= -20000 && account2.accountType.Equals(Account.AccountType.CREDIT))
                        {
                            Console.WriteLine("Работа с данным счетом запрещена т.к у вас есть кредитный счет суммой < -20000");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool checkHasAccountMethod(int clientId, int accountNumber)
        {
            foreach (Account account in accounts)
            {
                if (account.ClientId == clientId && account.AccountNumber == accountNumber)
                {
                    return true;
                }
            }
            Console.WriteLine("Указанный счет отсутствует в списке");
            return false;
        }

        public bool checkRecipientAccount(out int accountNumber2)
        {
            Console.WriteLine("Введите номер счета-получателя");
            accountNumber2 = Convert.ToInt32(Console.ReadLine());

            foreach (Account account in accounts)
            {
                if (account.AccountNumber == accountNumber2)
                {
                    return true;
                }
            }
            Console.WriteLine("Cчет Получателя отсутствует в списке");
            return false;
        }

        public bool checkHasUserMethod(int clientId)
        {
            bool checkHasAccountMethod=false;
            foreach (Account account in accounts)
            {
                if (account.ClientId == clientId)
                {
                    checkHasAccountMethod = true;
                }
            }
            if (checkHasAccountMethod==false)
            Console.WriteLine("Такого пользователя не сущесствует");
            return checkHasAccountMethod;
        }


        public void increaseBalance(int clientId, int accountNumber, double sum)
        {
            foreach (Account account in accounts)
            {
                if (account.AccountNumber == accountNumber && account.accountType.Equals(Account.AccountType.CURRENT) && sum > 1000000)
                {
                    sum = sum + 2000;
                    account.Balance += sum;
                    Console.WriteLine("Cчет №" + accountNumber + " " + "пополнен на сумму " + sum);
                }
                else if (account.AccountNumber == accountNumber)
                {
                    account.Balance += sum;
                    Console.WriteLine("Cчет №" + accountNumber + " " + "пополнен на сумму "+ sum);
                }
            }
        }

        public void decreaseBalance(int accountNumber, double sum)
        {
            if (sum > 0 && sum <= 30000)
            {
                foreach (Account account in accounts)
                {
                    if (account.AccountNumber == accountNumber && account.Balance >= sum)
                    {
                        account.Balance -= sum;
                        Console.WriteLine("Cчет №" + " " + accountNumber + " " + "уменьшен на сумму " + " " + sum);
                        break;
                    }
                    else if (account.AccountNumber == accountNumber)
                    {
                        account.Balance -= sum;
                        Console.WriteLine("Нельзя снять больше, чем есть на карте!");
                    }
                }
            }
            else if (sum <= 0)
            {
                Console.WriteLine("Сумма < 0");
            }
            else if (sum > 30000)
            {
                Console.WriteLine("Превышен лимит снятия > 30000 ");
            }

        }

        public void transfer(int clientId, int accountNumber, int accountNumber2, double sum)
        {
            if (accountNumber == accountNumber2)
            {
                Console.WriteLine("Нельзя перевести с одной и той же карты");
            }
            else if (sum < 0)
            {
                Console.WriteLine("Сумма перевода должна быть > 0");
            }
            else 
            {
                decreaseBalance(accountNumber, sum);
                increaseBalance(clientId, accountNumber2, sum);
                Console.WriteLine("Перевод на сумму: {0} + со счета {1} на счет {2} осуществлен",sum, accountNumber, accountNumber2);
            }
        }
    }
}
