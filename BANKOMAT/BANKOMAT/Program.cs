﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BANKOMAT
{
    class Program
    {
        static void Main(string[] args)
        {
            double operationSum;
            Boolean hasUser;
            Boolean hasDept;
            Boolean hasAccount;
            Boolean hasAccount2;
            int clientId;
            int accountNumber;
            int accountNumber2;

            Account account1 = new Account(3, 4412, -22222, Account.AccountType.CREDIT);
            Account account2 = new Account(1, 1111, 1000, Account.AccountType.DEBIT);
            Account account3 = new Account(1, 2222, -22222, Account.AccountType.CREDIT);
            Account account4 = new Account(7, 3233, 45000, Account.AccountType.CURRENT);
            Account account5 = new Account(3, 4444, -50000, Account.AccountType.DEBIT);
            Account account6 = new Account(1, 1111, 10000, Account.AccountType.DEBIT);
            Account account7 = new Account(2, 2223, 2020, Account.AccountType.CURRENT);
            Account account8 = new Account(8, 3333, 30320, Account.AccountType.DEBIT);
            Account account9 = new Account(2, 2211, 22100, Account.AccountType.CREDIT);
            Account account10 = new Account(4, 3334, 30500, Account.AccountType.CURRENT);

            Bankomat bank = new Bankomat();
            bank.addAccount(account1);
            bank.addAccount(account2);
            bank.addAccount(account3);
            bank.addAccount(account4);
            bank.addAccount(account5);
            bank.addAccount(account6);
            bank.addAccount(account7);
            bank.addAccount(account8);
            bank.addAccount(account9);
            bank.addAccount(account10);

            //bank.printAllAccounts();
            //Console.WriteLine("***********************************************");

            bank.identifyCustomer(out clientId, out accountNumber, out hasUser, out hasDept, out hasAccount);

            Console.WriteLine("Введите код операции из списка: 1 - Пополнить счет, 2 - Снять деньги с счета, 3 - Перести со счета на счет, 0 - Закрыть програму");
            int operationId = Convert.ToInt32(Console.ReadLine());

            switch (operationId)
            {
                case 1:
                    Console.WriteLine("Введите сумму пополнения");
                    operationSum = Convert.ToDouble(Console.ReadLine());
                    bank.increaseBalance(clientId, accountNumber, operationSum);
                    break;
                case 2:
                    Console.WriteLine("Введите сумму снятия");
                    operationSum = Convert.ToDouble(Console.ReadLine());
                    bank.decreaseBalance(accountNumber, operationSum);
                    break;
                case 3:
                    hasAccount2 = bank.checkRecipientAccount(out accountNumber2);
                    if (hasAccount2 == false)
                    {
                       break;
                    }
                    Console.WriteLine("Введите сумму перевода");
                    operationSum = Convert.ToDouble(Console.ReadLine());
                    bank.transfer(clientId, accountNumber, accountNumber2, operationSum);
                    break;
                case 0:
                    break;
                default:
                    Console.WriteLine("Нет такой операции");
                    break;

            }

            Console.WriteLine("Спасибо, пока!");
            //bank.printAllAccounts();

            Console.ReadLine();
        }
    }
}
