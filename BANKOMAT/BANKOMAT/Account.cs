﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BANKOMAT
{
    class Account
    {
        private int clientId;
        public int ClientId
        {
            get
            {
                return clientId;
            }
            set
            {
                if (value > 0)
                {
                    clientId = value;
                }
            }
        }

        private int accountNumber;
        public int AccountNumber
        {
            get
            {
                return accountNumber;
            }
            set
            {
                if (value > 0)
                {
                    accountNumber = value;
                }
            }
        }

        private double balance;
        public double Balance
        {
            get
            {
                return balance;
            }
            set
            {
                balance = value;
            }
        }

        public enum AccountType { CREDIT, CURRENT, DEBIT }
        public AccountType accountType;



        public Account(int clientId, int accountNumber, double balance, AccountType accountType)
        {
            ClientId = clientId;
            AccountNumber = accountNumber;
            Balance = balance;
            this.accountType = accountType;
        }

        public override string ToString()
        {
            return clientId + " " + accountNumber + " " + accountType.ToString() + " " + balance;
        }
    }
}
