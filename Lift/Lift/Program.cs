﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift
{
    class Program
    {
        static void Main(string[] args)
        {
            Lift lift = new Lift();
            Console.WriteLine("Введите кол-во этажей в доме");
            int countStages = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Выберите лифт \n 1 - queueLift \n 2 - stackLift");
            int chosenLift = Convert.ToInt32(Console.ReadLine());

            switch (chosenLift)
            {
                case 1:
                    {
                        lift.queueLift(countStages);
                        break;
                    }
                case 2:
                    {
                        lift.stackLift(countStages);
                        break;
                    }

                default:
                    break;
            }
        }
    }
}