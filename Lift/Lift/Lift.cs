﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift
{
    public class Lift
    {
        public int stage;
        public Queue<int> liftTask1;
        public Stack<int> liftTask2;

        public Lift()
        {
            liftTask1 = new Queue<int>();
            liftTask2 = new Stack<int>();
        }

        public void addTask1(int floor)
        {
            liftTask1.Enqueue(floor);
            Console.WriteLine("Добавилась задача уехать на этаж" + " " + floor);
        }
        public void addTask2(int floor)
        {
            liftTask2.Push(floor);
            Console.WriteLine("Добавилась задача уехать на этаж" + " " + floor);
        }

        public void doTask1()
        {
            liftTask1.Dequeue();
            Console.WriteLine("Задача выполнена");
        }

        public void doTask2()
        {
            liftTask2.Pop();
            Console.WriteLine("Задача выполнена");
        }

        public void queueLift(int countStages)
        {
            int currentFloor = 0;
            do
            {
                Console.WriteLine("Введите номер этажа от 1 до {0} или нажмите 0, если не хотите", countStages);
                int temp = Convert.ToInt32(Console.ReadLine());

                if (temp != 0)
                {
                    int chosedFloor = temp;
                    liftTask1.Enqueue(chosedFloor);
                }
                try
                {
                    for (int i = 0; i < 2; i++)
                    {
                        if (currentFloor == liftTask1.Peek())
                        {
                            doTask1();

                        }

                        if (liftTask1.Peek() > currentFloor)
                        {
                            Console.WriteLine("Текуший этаж = " + " " + currentFloor);
                            currentFloor++;
                            Console.WriteLine("Едем на этаж = " + " " + currentFloor);
                        }
                        else if (liftTask1.Peek() < currentFloor)
                        {
                            Console.WriteLine("Текуший этаж = " + " " + currentFloor);
                            currentFloor--;
                            Console.WriteLine("Едем на этаж = " + " " + currentFloor);
                        }
                        else
                        {
                            Console.WriteLine("Стоим на месте");
                        }
                        delayMethod();
                    }
                }
                catch (System.InvalidOperationException ioe)
                {
                    Console.WriteLine("Никуда не едем, очередь пуста, введите данные");
                    queueLift(countStages);
                }
            }
            while (true);
        }

        public void stackLift(int countStages)
        {
            int currentFloor = 0;
            do
            {
                Console.WriteLine("Введите номер этажа от 1 до {0} или нажмите 0, если не хотите", countStages);
                int temp = Convert.ToInt32(Console.ReadLine());

                if (temp != 0)
                {
                    int chosedFloor = temp;
                    liftTask2.Push(chosedFloor);
                }
                try
                {
                    for (int i = 0; i < 2; i++)
                    {
                        if (currentFloor == liftTask2.Peek())
                        {
                            doTask2();
                        }

                        if (liftTask2.Peek() > currentFloor)
                        {
                            Console.WriteLine("Текуший этаж = " + " " + currentFloor);
                            currentFloor++;
                            Console.WriteLine("Едем на этаж = " + " " + currentFloor);
                        }
                        else if (liftTask2.Peek() < currentFloor)
                        {
                            Console.WriteLine("Текуший этаж = " + " " + currentFloor);
                            currentFloor--;
                            Console.WriteLine("Едем на этаж = " + " " + currentFloor);
                        }
                        else
                        {
                            Console.WriteLine("Стоим на месте");
                        }
                        delayMethod();
                    }
                }
                catch (System.InvalidOperationException ioe)
                {
                    Console.WriteLine("Никуда не едем, очередь пуста, введите данные");
                    stackLift(countStages);
                }
            }
            while (true);
        }


        public void delayMethod()
        {
            System.Threading.Thread.Sleep(1000);
        }
    }
}
