﻿function updateTextBox(num) {
    if (num === 'C') {
        document.getElementById('textbox').value = '';
    }
    else if (num === 'CE') {
        var exp = document.getElementById('textbox').value;
        document.getElementById('textbox').value = exp.substring(0, exp.length - 1);
    }
    else {
        document.getElementById('textbox').value = document.getElementById('textbox').value + num;
    }
}
function calculate() {
    var firstNumber;
    var secondNumber;
    var exp = document.getElementById('textbox').value;

    if (exp.includes("+")) {
        var index = exp.indexOf('+');
        firstNumber = exp.substring(0, exp.length - index);
        secondNumber = exp.substring(index + 1, exp.length - 0);
        sum(firstNumber, secondNumber);
    }
    else if (exp.includes("-")) {
        var index = exp.indexOf('-');
        firstNumber = exp.substring(0, exp.length - index);
        secondNumber = exp.substring(index + 1, exp.length - 0);
        munus(firstNumber, secondNumber);
    }
    else if (exp.includes("*")) {
        var index = exp.indexOf('*');
        firstNumber = exp.substring(0, exp.length - index);
        secondNumber = exp.substring(index + 1, exp.length - 0);
        multiply(firstNumber, secondNumber);
    }
    else if (exp.includes("/")) {
        var index = exp.indexOf('/');
        firstNumber = exp.substring(0, exp.length - index);
        secondNumber = exp.substring(index + 1, exp.length - 0);
        divide(firstNumber, secondNumber);
    }
    else if (exp.includes("%")) {
        var index = exp.indexOf('%');
        firstNumber = exp.substring(0, exp.length - index);
        secondNumber = exp.substring(index + 1, exp.length - 0);
        ostatok(firstNumber, secondNumber);
    }
    else {
        updateTextBox('C');
    }
}
function multiply(num1, num2) {
    var result = parseFLoat(num1) * parseFLoat(num2);;
    document.getElementById('textbox').value = result;
}
function divide(num1, num2) {
    var result = parseFLoat(num1) / parseFLoat(num2);;
    document.getElementById('textbox').value = result;
}
function sum(num1, num2) {
    var result = parseFLoat(num1) + parseFLoat(num2);
    document.getElementById('textbox').value = result;
}
function minus(num1, num2) {
    var result = parseFLoat(num1) - parseFLoat(num2);
    document.getElementById('textbox').value = result;
}
function ostatok(num1, num2) {
    var result = parseFLoat(num1) % parseFLoat(num2);
    document.getElementById('textbox').value = result;
}