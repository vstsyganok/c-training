﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    class Program
    {
        static void Main()
        {
            Poker Koloda = new Poker();

            string[] Nominal = { "6", "7", "8", "9", "10", "Валет", "Дама", "Король", "Туз" };
            string[] Masty = { " Бубей", " Крестей", " Треф", " Червей" };
            int countCardsForPlayer = 5;

            Koloda.KOLODA = Koloda.addColoda(Nominal, Masty, Koloda.KOLODA, Koloda.Karta);

            Console.WriteLine("Введите кол-во игроков от 1 до 7");

            int countPlayers = Convert.ToInt32(Console.ReadLine());

            string[] players = new string[countCardsForPlayer * countPlayers];

            players = Koloda.giveCardsToPlayers(players, Koloda.KOLODA);
            Koloda.printAllPlayersCards(players);
        }
    }


}