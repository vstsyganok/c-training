﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    public class Poker
    {
        public string[,] Karta = new string[9, 4];
        public string[] Masty = new string[4];
        public string[] Nominal = new string[9];
        public string[] KOLODA = new string[36];

        public string[] addColoda(string[] Nominal, string[] Masty, string[] KOLODA, string[,] Karta)
        {
            for (int i = 0; i < 9; i++)
                for (int j = 0; j < 4; j++)
                {
                    Karta[i, j] = Nominal[i] + Masty[j];
                    KOLODA[i * 4 + j] = Convert.ToString(Karta[i, j]);
                }
            return KOLODA;
        }

        public string[] giveCardsToPlayers(string[] players, string[] KOLODA)
            {
                Random rand = new Random();

                for (int i = 0; i<players.Length; i++)
                {
                    String temp = KOLODA[rand.Next(0, 36)];

                    for (int j = 0; j <= i; j++)
                    {
                        if (players[j] == temp)
                        {
                            i--;
                            break;
                        }
                        else if (i == j)
                        {
                            players[i] = temp;
                        }
                    }
                }
                return players;
            }

        public void printAllPlayersCards(string[] players)
        {
            int l = 1;
            for (int k = 0; k < players.Length; k++)
            {
                if (k == 0 || k == 5 || k == 10 || k == 15 || k == 20 || k == 25 || k == 30 || k == 35)
                {
                    Console.WriteLine();
                    Console.WriteLine("Карты игрока № {0}: ", l);
                    l++;
                }
                Console.WriteLine(players[k]);
            }
            Console.ReadKey();
        }
    }
}
