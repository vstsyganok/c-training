﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {

            string cardNumber = enterCardNumber();
            int sum = calculateSum(cardNumber);
            printResolution(sum); 
            Console.ReadKey();
        }

        static string enterCardNumber()
        {
            Console.WriteLine("Введите номер вашей карты для проверки ее валидности");
            string cardName = Console.ReadLine().Replace(" ", string.Empty);

            if (String.IsNullOrEmpty(cardName))
            {
                return enterCardNumber();
            }
            return cardName;
        }
        static int calculateSum(string cardNumber)
        {
            int sum = 0;
            if (cardNumber.Length % 2 == 0)
            {
                for (int i = 0; i <= cardNumber.Length - 1; i++)
                {
                    if (i % 2 == 0)
                    {
                        int localValue = 2 * (int)Char.GetNumericValue(cardNumber[i]);

                        if (localValue > 9)
                        {
                            localValue = localValue - 9;
                        }

                        sum += localValue;
                    }
                    else if (i % 2 != 0)
                    {

                        sum += (int)Char.GetNumericValue(cardNumber[i]);
                    }
                }
            }
            else if (cardNumber.Length % 2 != 0)
            {

                for (int i = 0; i <= cardNumber.Length - 1; i++)
                {
                    if (i % 2 != 0)
                    {
                        int localValue = 2 * (int)Char.GetNumericValue(cardNumber[i]);

                        if (localValue > 9)
                        {
                            localValue = localValue - 9;
                        }

                        sum += localValue;
                    }
                    else if (i % 2 == 0)
                    {

                        sum += (int)Char.GetNumericValue(cardNumber[i]);
                    }
                }
            }
            return sum;
        }
        static void printResolution(int sum)
        {
            Console.WriteLine("Итоговая сумма по алгоритму Луна:" + sum);
            if (sum % 10 == 0)
            {
                Console.WriteLine("Карта валидна");
            }
            else if (sum % 10 != 0)
            {
                Console.WriteLine("Карта не валидна");
            }
        }
    }
}