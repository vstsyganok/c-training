﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Autotests1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AddCommentInBlog()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Navigate().GoToUrl("https://delivery-club.ru/");
            driver.Manage().Window.FullScreen();

            driver.FindElement(By.XPath("//div[@class='footer-menu']/a[.//h2[contains(.,'Блог')]]")).Click();
            driver.FindElement(By.Id("s")).SendKeys("Delivery Club: Топ-5 самых страшных и странных блюда мира" + Keys.Enter);
            driver.FindElement(By.XPath("//a[contains(.,'Delivery Club: Топ-5 самых страшных и странных блюда мира')]")).Click();
            driver.FindElement(By.Id("comment")).SendKeys("=)");
            driver.FindElement(By.Id("author")).SendKeys("Vladimir");
            driver.FindElement(By.Id("email")).SendKeys("tsyganokvs@gmail.com");
            driver.FindElement(By.Id("submit")).Click();
            driver.Quit();
        }

        [TestMethod]
        public void MakeAnOrder()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Manage().Window.FullScreen();
            driver.Navigate().GoToUrl("https://delivery-club.ru/");
            driver.FindElement(By.Id("user-addr__input")).SendKeys("Омск, Мельничная улица, 58В" + Keys.Enter);

            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//section[@class='vendor-item__title']/a[contains(.,'Ананас')]")).Click();

            IList<IWebElement> itemButton = driver.FindElements(By.XPath("//ul[@class='dish_list']/li//a[@class='button']"));
            IList<IWebElement> itemsPrice = driver.FindElements(By.XPath("//ul[@class='dish_list']/li//strong/span[1]"));
            IWebElement minPrice = driver.FindElement(By.XPath("//span[@class='js-vendor-headline-order']"));
            int i = 0;

            foreach (IWebElement item in itemsPrice)
            {
                int min = Convert.ToInt32(minPrice.Text);
                IWebElement currentSum = driver.FindElement(By.XPath("//div[@class='cart_2-ordered-title cart_2-ordered-title__visible not-only-bonus']/*//span[@class='basket_sum']"));
                int intCurrentSum;
                if (currentSum.Text.Equals(""))
                {
                    intCurrentSum = 0;
                }
                else
                {
                    intCurrentSum = Convert.ToInt32(currentSum.Text);
                }

                if (min > intCurrentSum)
                {
                    itemButton[i].Click();
                    i++;
                    continue;
                }
                break;
            }
            driver.FindElement(By.XPath("//div[@class='cart_2 from_basket']/a[@class='button alt open_basket from_basket']")).Click();
            Assert.IsTrue(((isElementPresent(driver, By.XPath("//div[@class='order-sum__not-enough']//a[contains(.,'Оформить заказ')]")))));
            driver.Quit();
        }

        [TestMethod]
        public void CheckValidationOnMainPage()
        {
            string correctValidateMessage = "Пожалуйста, уточните ваш адрес";

            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Navigate().GoToUrl("https://delivery-club.ru/");
            driver.Manage().Window.FullScreen();

            IWebElement textBox = driver.FindElement(By.Id("user-addr__input"));
            IWebElement findButton = driver.FindElement(By.XPath("//a[contains(.,'Найти рестораны')]"));

            if (!textBox.Text.Equals(""))
            {
                textBox.Clear();
            }
            findButton.Click();

            Thread.Sleep(5000);
            IWebElement validateMessage = driver.FindElement(By.XPath("//span[@class='tooltip-v2-par']/span[@class='tooltip-v2 tooltip-v2- tooltip-v2-addr-input tooltip-v2__error']"));

            Assert.AreEqual(correctValidateMessage, validateMessage.Text);
            driver.Quit();

        }


        [TestMethod]
        public void tryToOrderWithLowSum()
        {
            ChromeDriver driver = new ChromeDriver(@"E:\chromedriver");
            driver.Manage().Window.FullScreen();
            driver.Navigate().GoToUrl("https://delivery-club.ru/");
            driver.FindElement(By.Id("user-addr__input")).SendKeys("Омск, Мельничная улица, 58В" + Keys.Enter);

            Thread.Sleep(3000);
            driver.FindElement(By.XPath("//section[@class='vendor-item__title']/a[contains(.,'Ананас')]")).Click();

            IList<IWebElement> itemButton = driver.FindElements(By.XPath("//ul[@class='dish_list']/li//a[@class='button']"));
            IList<IWebElement> itemsPrice = driver.FindElements(By.XPath("//ul[@class='dish_list']/li//strong/span[1]"));
            IWebElement minPrice = driver.FindElement(By.XPath("//span[@class='js-vendor-headline-order']"));

            foreach (IWebElement item in itemsPrice)
            {
                int i = 0;

                int min = Convert.ToInt32(minPrice.Text);
                int current = Convert.ToInt32(itemsPrice[i].Text);

                if (min > current)
                {
                    itemButton[i].Click();
                    break;
                }
                i++;
            }

            driver.FindElement(By.XPath("//div[@class='cart_2 from_basket']/a[@class='button alt open_basket from_basket']")).Click();
            Assert.IsTrue(((!isElementPresent(driver, By.XPath("//div[@class='not-enough-min-sum']//a[contains(.,'ОФОРМИТЬ ЗАКАЗ')]")))) && (isElementPresent(driver, By.XPath("//div[@class='not-enough-min-sum']//a[contains(.,'В МЕНЮ РЕСТОРАНА')]"))));
        }

        public bool isElementPresent(ChromeDriver driver, By by)
        {
            Thread.Sleep(5000);
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}

